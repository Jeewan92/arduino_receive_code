//function to send request to modbus slave ID 4 for getting flow rate data
void MbusSlave4(){
  // Variable decleration for slave ID 4
  uint16_t result;
  result = node.readHoldingRegisters(0x0001,2);
  if (result == node.ku8MBSuccess)
  {
    Serial.println("Slave Id 4");
    u.Id4[1] = node.getResponseBuffer(0x00);
    u.Id4[0] = node.getResponseBuffer(0x01);//1000.0f);
    Serial.println(u.inlet4);
  }

} 
