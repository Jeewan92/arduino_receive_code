//This library is for DS3231 RTC timer
#include <RTClib.h>
//Since the Arduino nano has only one Hardware Serial Communication (rx0,tx1), so this hardware Serial is kept for debuging perposes 
//to communicate with UART. So, we import the SoftwareSerial library to make the digital GPIO as Serial communication. 
#include <SoftwareSerial.h>

//This library is for modbus communication protocol and this one is for modbus Master.
#include <ModbusMaster.h>

//Initializing the DS3231 RTC timmer
RTC_DS3231 rtc;

//Initilizing the Rx TX pin for Modbus communication
SoftwareSerial mySerial(4, 5); // RX, TX

//Initializing the Rx Tx pin for lora communication
SoftwareSerial mySerial2(2, 3); // RX, TX

//Initializing the modbus object for two different modbus slave as node and node1
ModbusMaster node;
ModbusMaster node1;

//defining DE - Driver Enabe and RE - Recieve Enable pin
#define MAX485_RE_NEG  8

//defining Interrupt pin to control the opening positioning of the ball valve 
#define INTERRUPT_PIN 10

//defining the pin to control the rellay to give interrupt to the INTERRUPT_PIN
#define RELAY_PIN_FOR_INTERRUPT_PIN 9

//defining the pin for reading the potentiometer value of the ball valve
#define FEED_BACK A0


//This are for defining the function prototype of the multiple Tabs
void FlowMeter();
void transmit();
void lengthofFrame();
void ValveControl();
void MbusSlave117();
void MbusSlave4();
void ModbusSlave4TF();
void Concate();
void Recieve();

//defining the char array for storing the value of two Sensor Flow meter and Total flow meter
char Flowmeter[8];
char Flowmeter1[8];
char Flowmeter2[8];

char Totalflow[8];
char Totalflow1[8];
char Totalflow2[8];

char ValvePosition[1]= {"1"};
char ValvePosition1[1]= {"1"};
char ValvePosition2[1]= {"1"};

char Time[20];

//defining the char array for storing the all sensors data and the special text to transmit to lora master. 
//char Payload[90];
int TpayloadLength;

//defining and assigning the float variable to replicate the sensors value
float Fm = 133.133;
float Fm1 = 123.222;
float Fm2 = 143.662;

float TF =1233.92;
float TF1 = 1233.56;
float TF2 = 7000.56;


//Defining the variable and byte array for transmit farme of Lora.
long XOR = 0;// defining the long variable to holds the XOR hex sum of Frame type + Frame ID +  + command Type + DSD + LengthOfFrame + size of payload + Payload

//FFC-Frame Type (05), Frame Number/ID (00),Command Type (01)
byte FFC[3] = {0x05,0x00,0x01};

//DSD- Destination Address (04 00),ACK response(00),Send Radius(07),Discovery Routing (01)
byte DASD[5] = {0x04,0x00,0x00,0x07,0x01};

//Lora Recieve frame explaination
//05 00 82 08 10 00 43 04 EE EE EE EE D8
//05 - frame type -Application data Used by the networking protocol application layer to use the interface.
// 00 - Frame number -The frame sequence number field is currently unused and the value is fixed at 0x00.
// 82 - Command Type -Reading Configuration Information Response
// 08 - length in byte till before error check 
// 10 00 - address of the sender so MSB of address is recieved first and then LSB (the actual address is 00 10)
// 43 - Field strength -The smaller the value the more the signal strong
// 04 - Load length -length of payload -max is up to 128 byte or 0xFF
// EE EE EE EE - payload recieved
//D8 - frame tail or checksum -The frame end field is a 1-byte XOR check. This check value is the first byte from the frame header (frame type byte)
                               //The result of an exclusive OR operation of all bytes before starting to check the byte. Check all bytes in the full frame XOR operation
                               //If it is 0, the verification is correct.

// Declearing variable for 0x82 lora recieve frame
byte frameType; //
byte frameNumber; 
byte commandType;
byte lengthOfbyte;
byte senderAddress[1];
byte fieldStrength;
byte loadLength;
int payloadLength; //declearing the int variable to store converted byte loadlength to int for reading the corresponding equivalent buffer length  
char Control[2]; // to store the control commad i - opening the valve if it is greater than the current position.
char Recieve_payload[3]; //to store the percentage of vale opening and closing value
int recievedData; //to store the converted char Recieve_payload to integer for converting to base 1023
byte frameTail;
int calculatedPosition = 0; // this will store the calculated position to base 1023

//global variable for valve 
//this millis is for controlling interrupt signal
unsigned long previousMillis = 0;  //will store last time 
const long period = 10000;  //keeping 10 sec cusion time so that the opening of ball valve will complete the opening and stops for next interrupt signal.


int posiTionValue; // this variable is for storing the ponentiometer feedback from ball valve.

//declearing variable for transmiting frame for lora
unsigned long transmitPreviousMillis = 0;
const long transmitPeriod = 5000; //this value will control when to transmit the lora frame, this one is set to 5 minuates.

//declearing variable for modbus request
unsigned long modbusPreviousMillis = 0;  //will store last time 

//declearing variable for RTC clock 
String dateTime = "12/03/2022 12:30:34";


//converting the uint16_t to float
//this one for flow data
union{
  uint16_t Id4[2];
  float inlet4;
  
}u;
//this one is for total flow
union{
  uint16_t Id4TFM[2];
  float inlet4TFM;
}v;


union{
  uint16_t Id8[2];
  float inlet8;
  
}t;

/*for modbus*/
void preTransmission()
{ 
  mySerial.listen();
  digitalWrite(MAX485_RE_NEG, 1);
  
}
void postTransmission()
{ 
  mySerial.listen();
  digitalWrite(MAX485_RE_NEG, 0);
  
}


void setup() {
  
  Serial.begin(9600);
  //defining the DE n RE controller pin as output
  pinMode(MAX485_RE_NEG, OUTPUT);
 
  //pin configuration for motor valve control
  pinMode(INTERRUPT_PIN,OUTPUT);
  pinMode(RELAY_PIN_FOR_INTERRUPT_PIN,OUTPUT);
  pinMode(FEED_BACK,INPUT);

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    //while (1) delay(10);
  }
  
  if (rtc.lostPower()) {
    Serial.println("RTC is NOT running, let's set the time!");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  //pulling the DE n RE pin to low so that we make sure the Max485 is in recieve mode while seting up for first time
  digitalWrite(MAX485_RE_NEG, 0);
  
  //for keeping the interrupt signal for ball valve always HIGH to and interrupt will be set by relay control signal
  digitalWrite(INTERRUPT_PIN,HIGH);

  //setting baud rate for software serial mySerial object
  mySerial.begin(9600);

  //setting baud rate for software serial mySerial2 object
  mySerial2.begin(9600);

  //setting serial communication and slave id for modbus object node
  node.begin(4, mySerial);
  
  //setting the serial communication and slave id for modbus object node1
  node1.begin(8,mySerial);

  //calling the pretransmission and posttransmission function by modbus node object
  node.preTransmission(preTransmission);
  node.postTransmission(postTransmission);

  //calling the pretransmission and posttransmission function by modbus node1 object
  node1.preTransmission(preTransmission);
  node1.postTransmission(postTransmission);

}

void loop()
{ 
  //declearing and assigning the current millis for transmitting the lora transmit frame
  unsigned long transmitCurrentMillis = millis();

  //declearing and assigning the current millis for sending modbus request
  unsigned long modbusCurrentMillis = millis();

  //setting the time for DS3231 RTC timmer
  DateTime time = rtc.now();

  //call the MbusSlave4 function every 2 minutes
  if (modbusCurrentMillis - modbusPreviousMillis >= 120000){
  MbusSlave4();
  }

  //call the ModbusSlave4TF function after every 2 minuates 10 miliseconds
  if (modbusCurrentMillis - modbusPreviousMillis >= 120010){
   ModbusSlave4TF();//since once request is not able to get 100% response so,send request atleast twice.
   ModbusSlave4TF();

   //call Full Timestamp
   dateTime = String(time.timestamp(DateTime::TIMESTAMP_FULL));
   Serial.println(dateTime);
   Serial.println(sizeof(dateTime));
   modbusPreviousMillis = modbusCurrentMillis;
   
  }
  //call the transmit function every after 5 minuates
  if (transmitCurrentMillis - transmitPreviousMillis >= transmitPeriod)
  {
      //calling FlowMeter function
      FlowMeter();
      mySerial2.listen(); //we need to call listern function of software serial to switch from one softwareserial abject to another.
      transmit();
      transmitPreviousMillis = transmitCurrentMillis;
  }
  mySerial2.listen();
  Recieve(); //keep on listerning to lora recieve frame or keep on checking the recieve buffer of mySerial2 object.
  ValveControl(); //keep on controlling the valve depending on lora recieved payload.
}


     
