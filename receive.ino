void RecieveData()
{ 
  static boolean recvInProgress = false; // when this is false and if there is any data in recieve buffer it will read the a byte from input buffer and check if it is start marker
  static byte ndx = 0; // this variable is defined to take care of the index and the number of bytes to read

  byte startMarker = 0x05;
  byte endMarker = 0x00;

  static byte xOR;
  byte rb; // to store a byte when it is read from the recieve buffer.

  while (mySerial1.available() > 0 && newData == false) 
  {
    rb = mySerial1.read();
    Serial.println(rb);

    if (recvInProgress == true) 
    {
      xOR = xOR ^ rb;
      if (xOR != endMarker) 
      {
        receivedBytes[ndx] = rb;
        Serial.print(receivedBytes[ndx],HEX);
        Serial.print(" ");
        ndx++;
        if (ndx >= maxReadBytes) 
        {
          ndx = maxReadBytes - 1;
        }
      } 
      else 
      {
        receivedBytes[ndx] = '\0'; // terminate the string
        Serial.println();

        recvInProgress = false;
        byteReceivednum = ndx;  // save the number for use when printing
        ndx = 0;
        //newData = true;
        //ParseRecieveData();
      }
    } 
    else if (rb == startMarker) 
    {
      xOR = startMarker;
      recvInProgress = true;
    }
  } // Add the missing curly bracket here
}
