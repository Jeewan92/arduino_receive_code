//function sending request to modbus salve 4 for tolal flow data
void ModbusSlave4TF()
{ 
  uint16_t resultTFM;
  resultTFM = node.readHoldingRegisters(0x0127,2);
  
  if (resultTFM == node.ku8MBSuccess)
  {
    Serial.println("Slave Id 4 TFM");
    v.Id4TFM[1]=node.getResponseBuffer(0x00);
    v.Id4TFM[0]=node.getResponseBuffer(0x01);

    Serial.println(v.inlet4TFM);
  }
}
