//function to transmit the lora frame to lora master
void transmit() {
  char a[1] ={"A"};
  char b[1] ={"B"};
  char c[1] = {"C"};
  char d[1] = {"D"};
  char e[1] = {"E"};
  char f[1] = {"F"};
  char g[1] = {"G"};
  char h[1] = {"H"};
  char i[1] = {"I"};
  char j[1] = {"J"};
  for(int i = 0; i<3; i++)
  {
    mySerial2.write(FFC[i]);
    XOR = XOR ^ FFC[i];
    Serial.print(FFC[i],HEX);
    Serial.print(" ");
  }
 
 lengthofFrame();
  
 
 for(int j = 0; j<5; j++)
 {
  mySerial2.write(DASD[j]);
  XOR = XOR ^ DASD[j];
  Serial.print(DASD[j],HEX);
  Serial.print(" ");
 }
 mySerial2.write(TpayloadLength);
 XOR = XOR ^ TpayloadLength;
 Serial.print(TpayloadLength,HEX);
 Serial.print(" ");

 mySerial2.write(a[0]);
 XOR = XOR ^ a[0];
 Serial.print(a[0],HEX);
 Serial.print(" ");
 mySerial2.write(sizeof(Time));
 Serial.print(sizeof(Time),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Time);

 
 for (int k = 0; k<sizeof(Time); k++)
 {
  mySerial2.write(Time[k]);
  Serial.print(Time[k],HEX);
  Serial.print(" ");
  XOR = XOR ^ Time[k];
 }

 mySerial2.write(b[0]);
 XOR = XOR ^ b[0];
 Serial.print(b[0],HEX);
 Serial.print(" ");
 mySerial2.write(sizeof(Flowmeter));
 Serial.print(sizeof(Flowmeter),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Flowmeter);
 for (int ka = 0; ka<sizeof(Flowmeter); ka++)
 {
  mySerial2.write(Flowmeter[ka]);
  Serial.print(Flowmeter[ka],HEX);
  Serial.print(" ");
  XOR = XOR ^ Flowmeter[ka];
 }

 mySerial2.write(c[0]);
 XOR = XOR ^ c[0];
 Serial.print(c[0],HEX);
 Serial.print(" ");
 mySerial2.write(sizeof(Flowmeter1));
 Serial.print(sizeof(Flowmeter1),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Flowmeter1);
 for(int kb = 0; kb < sizeof(Flowmeter1); kb++)
 {
  mySerial2.write(Flowmeter1[kb]);
  Serial.print(Flowmeter1[kb], HEX);
  Serial.print(" ");
  XOR = XOR ^ Flowmeter1[kb];
 }

 mySerial2.write(d[0]);
 Serial.print(d[0], HEX);
 Serial.print(" ");
 XOR = XOR ^ d[0];
 mySerial2.write(sizeof(Flowmeter2));
 Serial.print(sizeof(Flowmeter2),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Flowmeter2);
 for(int kc = 0; kc < sizeof(Flowmeter2); kc++)
 {
  mySerial2.write(Flowmeter[kc]);
  Serial.print(Flowmeter[kc],HEX);
  Serial.print(" ");
  XOR = XOR ^ Flowmeter[kc];
 }

 mySerial2.write(e[0]);
 Serial.print(e[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ e[0];
 mySerial2.write(sizeof(Totalflow));
 Serial.print(sizeof(Totalflow),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Totalflow);
 for(int kd = 0; kd < sizeof(Totalflow); kd++)
 {
  mySerial2.print(Totalflow[kd]);
  Serial.print(Totalflow[kd],HEX);
  Serial.print(" ");
  XOR = XOR ^ Totalflow[kd]; 
 }

 mySerial2.write(f[0]);
 Serial.print(f[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ f[0];
 mySerial2.write(sizeof(Totalflow1));
 Serial.print(sizeof(Totalflow1),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Totalflow1);
 for(int ke = 0; ke < sizeof(Totalflow1);ke++)
 {
  mySerial2.write(Totalflow1[ke]);
  Serial.print(Totalflow1[ke],HEX);
  Serial.print(" ");
  XOR = XOR ^ Totalflow1[ke];
 }

 mySerial2.write(g[0]);
 Serial.print(g[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ g[0];
 mySerial2.write(sizeof(Totalflow2));
 Serial.print(sizeof(Totalflow2),HEX);
 Serial.print(" ");
 XOR = XOR ^ sizeof(Totalflow2);
 for(int kf = 0; kf < sizeof(Totalflow2);kf++)
 {
  mySerial2.write(Totalflow2[kf]);
  Serial.print(Totalflow2[kf],HEX);
  Serial.print(" ");
  XOR = XOR ^ Totalflow2[kf];
 }

 mySerial2.write(h[0]);
 Serial.print(h[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ h[0];
 mySerial2.write(ValvePosition[0]);
 Serial.print(ValvePosition[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ ValvePosition[0];

 mySerial2.write(i[0]);
 Serial.print(i[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ i[0];
 mySerial2.write(ValvePosition1[0]);
 Serial.print(ValvePosition1[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ ValvePosition1[0];

 mySerial2.write(j[0]);
 Serial.print(j[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ j[0];
 mySerial2.write(ValvePosition2[0]);
 Serial.print(ValvePosition2[0],HEX);
 Serial.print(" ");
 XOR = XOR ^ ValvePosition2[0];

 
mySerial2.write(XOR);
mySerial2.flush();
Serial.print(XOR,HEX);

XOR = 0;
Serial.println();     
}
